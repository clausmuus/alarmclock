const events = require('./events')
const fsp = require('fs').promises
const settingsdir = __filename.replace('.js', '')
var settings = {load, save}

async function load() {
	Object.keys(settings).forEach(key => delete settings[key])
	settings.load = load
	settings.save = save
	mergeObjects(settings, JSON.parse(await fsp.readFile(`${settingsdir}/defaults.json`, 'utf8')))
	try {
		mergeObjects(settings, JSON.parse(await fsp.readFile(`${settingsdir}/local.json`, 'utf8')))
	} catch(e) {
		console.log(`The settings file '${settingsdir}/local.json' is corrupt`)
	}
}

async function save(setting) {
	try {
		setting = mergeObjects(JSON.parse(await fsp.readFile(`${settingsdir}/local.json`, 'utf8')), setting)
	} catch(e) {
		console.log(`The settings file '${settingsdir}/local.json' is corrupt`)
	}
	await fsp.writeFile(`${settingsdir}/local.json`, JSON.stringify(setting, null, '\t'))
	mergeObjects(settings, setting)
	events.emit('settings')
}

function mergeObjects(to, from) {
	for (let key in from) {
		if (from[key] && ! (from[key] instanceof Array) && typeof from[key] == 'object' && typeof to[key] == 'object') {
			mergeObjects(to[key], from[key])
		} else {
			to[key] = from[key]
		}
	}
	return to
}

module.exports = settings