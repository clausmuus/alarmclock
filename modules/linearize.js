module.exports = (value, max, linearize) => {
	value = Math.max(0, Math.min(value, max))
	linearize = (linearize>=0) ? (linearize/10.0+1.0) : (1.0/((-linearize/10.0)+1.0))
	return max - Math.pow(1 - Math.pow(value / max, linearize), 1 / linearize) * max
}
