const settings = require('../modules/settings')
const events = require('../modules/events')
const rpio = require('rpio')
const r1wt = require('raspi-1wire-temp')

module.exports = class {
	constructor(display) {
		this.display = display
		let setting = settings.temperatue_sensor

		rpio.init({mapping: 'gpio'})
		rpio.pud(setting.data_pin, rpio.PULL_UP)
		if (setting.ground_pin) {
			rpio.open(setting.ground_pin, rpio.OUTPUT)
			rpio.write(setting.ground_pin, rpio.LOW)
		}
		if (setting.vcc_pin) {
			rpio.open(setting.vcc_pin, rpio.OUTPUT)
			rpio.write(setting.vcc_pin, rpio.HIGH)
		}

		setTimeout(() => {
			let last_temperature = 0
			let devices = r1wt.findDevices()
			if (devices.length) {
				let device = r1wt.fromDevice(devices[0])
				setInterval(async () => {
					try {
						let temperature = ((await device.current.catch(() => {})) || {}).celsius
						if (temperature !== undefined && Math.round(temperature*5) != Math.round(last_temperature*5)) {
							last_temperature = temperature
							temperature = Math.round(temperature*10) / 10 + setting.offset
							events.emit('temperatures', {local: temperature})
						}
					} catch(e) {}
				}, 1000)
			}
		}, 1000)
	}
}
