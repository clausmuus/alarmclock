const settings = require('../modules/settings')
const express = require('express');

module.exports = class {
	constructor() {
		const port = process.env.PORT || 80
		const server = express();

		server.use(express.static(__dirname + '/../web'))
		server.get('/', (req, res) => {
			res.sendFile(__dirname + '/../web/index.html')
		})
		server.get('/settings', (req, res) => {
			res.status(200).send(settings)
		})
		server.put('/settings', async (req, res) => {
			console.log(req.body)
			res.status(200)
		})

		server.listen(port, () => {
			console.log(`  Listen on port ${port}`)
		})
	}
}
