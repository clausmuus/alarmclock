const settings = require('../modules/settings')
const events = require('../modules/events')
const readline = require('readline')
const process = require('process')

module.exports = class {
	constructor() {
		if (process.argv.indexOf('--console') == -1) {
			console.log('  Use argument --console to activate input and output from/to console')
			return
		}

		readline.createInterface(process.stdin, process.stdout)

		this.display = {
			getWidth: () => 20,
			getHeight: () => 5,

			clear: () => {
				readline.cursorTo(process.stdout, 0, 0)
				readline.clearScreenDown(process.stdout)
			},

			text: function(style, text, col = 1, row = 1, active = false, wrap = false) {
				let align = (col > 0 ? 1 : (col < 0 ? -1 : 0))
				let left = Math.round((col - align) * align + (align == 1 ? 0 : (align == -1 ? Math.max(0, this.getWidth() - text.length) : Math.max(0, this.getWidth()/2 - text.length/2))))
				let top = Math.round((row - 1) + Math.ceil((settings.styles[style].top || 0) / (settings.styles[style].height || 1)))

				readline.cursorTo(process.stdout, left, top)
				process.stdout.write(style == 'Title' || style == 'Time' ? '\x1B[1;31m' : '\x1B[0m')
				process.stdout.write(text)

				if (active) {
					let draw = () => {
						active = !active
						readline.cursorTo(process.stdout, left, top)
						process.stdout.write(active ? '\x1B[0;33m' : '\x1B[0m')
						process.stdout.write(text)
						this.active_timer = setTimeout(draw, active ? 300 : 50)
					}
					draw()
				}
			},

			drawRectangleSize: (color, border, left, top, width, height) => {
				left = Math.round(left/(settings.styles['Text'].width || 8))
				top = Math.round(top/(settings.styles['Text'].height || 8))

				readline.cursorTo(process.stdout, left, top)
				process.stdout.write('='.repeat(width))
			},

			drawPoints: () => {}
		}

		events.on('render', call => {
			clearTimeout(this.display.active_timer)
			call(this.display)
			process.stdout.write('\x1B[0m')
			readline.cursorTo(process.stdout, 0, this.display.getHeight())
		})

		process.stdin.on('keypress', (_, key) => {
			// console.log(key.name)
			switch(key.name) {
				case 'return':
					events.emit('input', 'enter')
					break
				case 'backspace':
					events.emit('input', 'back')
					break
				case 'left':
					events.emit('input', 'wheel', -1)
					break
				case 'right':
					events.emit('input', 'wheel', 1)
					break
			}
		})
	}
}
