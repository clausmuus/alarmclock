const events = require('../modules/events')

module.exports = class {
	constructor() {
		this.timer = 10
		this.active = false

		events.on('view', name => {setTimeout(() => {
			this.active = name == 'timer'
			if (this.active) {
				if (!this.timer) {
					this.timer = 10
				}
				this.show()
			}
		}, 100)})

		events.on('input', (type, value) => {
			if (!this.active) return
			switch(type) {
				case 'enter':
					// clearTimeout(this.edit_timer)
					this.active = false
					events.emit('timer', this.timer)
					events.emit('view', 'clock')
					break
				case 'back':
					// clearTimeout(this.edit_timer)
					this.active = false
					events.emit('view', 'clock')
					break
				case 'wheel':
					this.timer = Math.max(0, Math.min(this.timer + value * 5, 180))
					this.show()
					break
			}
		})
	}

	async show() {
		events.emit('render', display => {
			if (!this.active) return
			display.clear()
			display.text('Title', 'Kurzzeitwecker')

			display.text('Text', this.timer ? `In ${this.timer} Minuten wecken` : 'Ausgeschaltet', 1, 1, true)
		})
	}
}
