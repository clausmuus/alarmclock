const events = require('../modules/events')

module.exports = class {
	constructor() {
		this.sleep = 60
		this.active = false

		events.on('view', name => {setTimeout(() => {
			this.active = name == 'sleep'
			if (this.active) {
				this.show()
			}
		}, 100)})

		events.on('input', (type, value) => {
			if (!this.active) return
			switch(type) {
				case 'enter':
					events.emit('sleep', this.sleep)
					events.emit('view', 'radio')
					break
				case 'back':
					events.emit('view', 'clock')
					break
				case 'wheel':
					this.sleep = Math.max(5, Math.min(this.sleep + value * 5, 180))
					this.show()
					break
			}
		})
	}

	async show() {
		events.emit('render', display => {
			display.clear()
			display.text('Title', 'Ausschalttimer')

			display.text('Text', `In ${this.sleep} Minuten ausschalten`, 1, 1, true)
		})
	}
}
