const settings = require('../modules/settings')
const events = require('../modules/events')
const date = require('date-and-time')

module.exports = class {
	constructor() {
		this.active = false
		this.playing = false

		events.on('view', name => {setTimeout(() => {
			this.show_volume = false
			this.active = name == 'alarm'
			if (this.active) {
				this.volume = 0
				this.play()
				this.show()
			} else {
				clearTimeout(this.show_volume_timer)
			}
		}, 100)})

		events.on('update', () => {
			if (this.active) {
				this.show()
			}
			let now = new Date()
			let alarm = settings.alarm.times.find(alarm => alarm.active && alarm.days[(now.getDay()+6)%7] && alarm.hour == now.getHours() && alarm.minute == now.getMinutes())
			if (alarm && alarm != this.alarm) {
				this.alarm = alarm
				events.emit('view', 'alarm')
			}
		})

		events.on('input', (type, value) => {
			if (!this.active) return
			clearTimeout(this.show_volume_timer)
			switch(type) {
				case 'enter':
					clearTimeout(this.increase_volume_timer)
					clearTimeout(this.sleep_timer)
					clearTimeout(this.off_timer)
					if (this.playing) {
						this.playing = false
						this.volume = 0
						events.emit('stop')
						this.sleep_timer = setTimeout(this.play.bind(this), 1000 * settings.alarm.sleep)
					} else {
						this.volume = settings.alarm.volume
						this.play()
					}
					break
				case 'back':
					this.stop()
					break
				case 'wheel':
					if (this.playing) {
						clearTimeout(this.increase_volume_timer)
						this.volume = Math.max(0, Math.min(this.volume + value, settings.volume.steps))
						this.show_volume = true
						this.show_volume_timer = setTimeout(() => {this.show_volume = false; this.show()}, 1000)
						events.emit('volume', this.volume)
						this.show()
					}
					break
			}
		})

		events.on('timer', time => {
			clearTimeout(this.timer_timer)
			if (time) {
				this.timer_timer = setTimeout(() => {
					events.emit('view', 'alarm')
				}, 1000*60*time)
			}
		})
	}

	async show() {
		events.emit('render', display => {
			display.clear()
			display.text('Time', date.format(new Date(), 'H:mm'), 0)

			if (this.show_volume) {
				let width = display.getWidth()
				display.drawRectangleSize(1, 1, 0, 45, width / settings.volume.steps * this.volume, 2)
			}

			display.text('Text', this.alarm ? `${this.alarm.hour}:${this.alarm.minute.toString().padStart(2, '0')} Alarm` : 'Alarm', 0, 3)
		})
	}

	play() {
		if (!this.playing) {
			this.playing = true
			events.emit('volume', this.volume)
			events.emit('play', settings.alarm.station)
			if (!this.volume) {
				let increase_volume = () => {
					this.increase_volume_timer = setTimeout(() => {
						this.volume++
						events.emit('volume', this.volume)
						if (this.volume < settings.alarm.volume) {
							increase_volume()
						}
					}, 1000 * settings.alarm.increase_time / settings.alarm.volume)
				}
				increase_volume()
			}
			clearTimeout(this.off_timer)
			this.off_timer = setTimeout(this.stop, 1000 * settings.alarm.off)
		}
	}

	stop() {
		clearTimeout(this.increase_volume_timer)
		clearTimeout(this.sleep_timer)
		clearTimeout(this.off_timer)
		this.playing = false
		this.volume = 0
		events.emit('stop')
		if (this.alarm && this.alarm.active === 1) {
			// Einmal Alarm löschen
			settings.alarm.times.splice(settings.alarm.times.findIndex(alarm => alarm === this.alarm), 1)
			settings.save({alarm:{times: settings.alarm.times}})
		}
		events.emit('view', 'clock')
		setTimeout(() => {this.alarm = null}, 1000 * 60)
	}
}
