const settings = require('../modules/settings')
const events = require('../modules/events')

module.exports = class {
	constructor() {
		this.active = false
		this.edit = false

		this.setting_options = [
			{
				name: 'Sender für Radio',
				set: (inc) => {
					settings.radio.station = Math.max(0, Math.min(settings.radio.station + inc, settings.stations.filter(station => !station.ringtone).length - 1))
					events.emit('volume', settings.radio.volume)
					events.emit('play', settings.stations.map((station, id) => ({...station, id})).filter(station => !station.ringtone)[settings.radio.station].id)
				},
				save: () => {
					settings.save({radio: {station: settings.radio.station}})
					events.emit('stop')
				},
				value: () => settings.stations.filter(station => !station.ringtone)[settings.radio.station].name
			},
			{
				name: 'Lautstärke für Radio',
				set: (inc) => {
					settings.radio.volume = Math.max(0, Math.min(settings.radio.volume + inc, settings.volume.steps))
					events.emit('volume', settings.radio.volume)
					events.emit('play', settings.stations.map((station, id) => ({...station, id})).filter(station => !station.ringtone)[settings.radio.station].id)
				},
				save: () => {
					settings.save({radio: {volume: settings.radio.volume}})
					events.emit('stop')
				},
				value: () => Math.round(settings.radio.volume * 100 / settings.volume.steps) + ' %'
			},
			{
				name: 'Sender beim Wecken',
				set: (inc) => {
					settings.alarm.station = Math.max(0, Math.min(settings.alarm.station + inc, settings.stations.length - 1))
					events.emit('volume', settings.alarm.volume)
					events.emit('play', settings.alarm.station)
				},
				save: () => {
					settings.save({alarm: {station: settings.alarm.station}})
					events.emit('stop')
				},
				value: () => settings.stations[settings.alarm.station].name
			},
			{
				name: 'Lautstärke beim Wecken',
				set: (inc) => {
					settings.alarm.volume = Math.max(0, Math.min(settings.alarm.volume + inc, settings.volume.steps))
					events.emit('volume', settings.alarm.volume)
					events.emit('play', settings.alarm.station)
				},
				save: () => {
					settings.save({alarm: {volume: settings.alarm.volume}})
					events.emit('stop')
				},
				value: () => Math.round(settings.alarm.volume * 100 / settings.volume.steps) + ' %'
			},
			{
				name: 'Pause beim Wecken',
				set: (inc) => {settings.alarm.sleep = Math.max(0, Math.min(settings.alarm.sleep + inc * 60, 15 * 60))},
				save: () => {settings.save({alarm: {sleep: settings.alarm.sleep}})},
				value: () => (settings.alarm.sleep / 60) + ' Minuten'
			},
			{
				name: 'Dauer des Weckens',
				set: (inc) => {settings.alarm.off = Math.max(0, Math.min(settings.alarm.off + inc * 60, 180 * 60))},
				save: () => {settings.save({alarm: {sleep: settings.alarm.off}})},
				value: () => (settings.alarm.off / 60) + ' Minuten'
			},
		]

		events.on('view', name => {setTimeout(() => {
			this.active = name == 'settings'
			if (this.active) {
				this.setting = 0
				this.show()
			}
		}, 100)})

		events.on('input', (type, value) => {
			if (!this.active) return
			switch(type) {
				case 'enter':
					if (this.edit) {
						this.setting_options[this.setting].save()
					} else {
						this.setting_options[this.setting].set(0)
					}
					this.edit = !this.edit
					this.show()
					break
				case 'back':
					if (this.edit) {
						this.setting_options[this.setting].save()
						this.edit = false
						this.show()
					} else {
						events.emit('view', 'clock')
					}
					break
				case 'wheel':
					if (this.edit) {
						this.setting_options[this.setting].set(value)
					} else {
						this.setting = Math.max(0, Math.min(this.setting + value, this.setting_options.length - 1))
					}
					this.show()
					break
			}
		})
	}

	show() {
		events.emit('render', display => {
			display.clear()

			display.text('Title', 'Einstellungen')

			display.text('Text', this.setting_options[this.setting].name)
			display.text('Text', this.setting_options[this.setting].value(), 1, 2, this.edit)
		})
	}
}
