const settings = require('../modules/settings')
const events = require('../modules/events')
const rpio = require('rpio')
const Gpio = require('onoff').Gpio

module.exports = class {
	constructor() {
		this.setting = settings.input
		this.last_time = 0
		this.back_timer = 0
		this.state = []

		rpio.init({mapping: 'gpio'})
		if (this.setting.ground_pin) {
			rpio.open(this.setting.ground_pin, rpio.OUTPUT)
			rpio.write(this.setting.ground_pin, rpio.LOW)
		}
		if (this.setting.vcc_pin) {
			rpio.open(this.setting.vcc_pin, rpio.OUTPUT)
			rpio.write(this.setting.vcc_pin, rpio.HIGH)
		}

		rpio.pud(this.setting.button_pin, rpio.PULL_UP)
		rpio.pud(this.setting.rotary_pin_1, rpio.PULL_UP)
		rpio.pud(this.setting.rotary_pin_2, rpio.PULL_UP)
		new Gpio(this.setting.button_pin, 'in', 'both').watch((err, state) => {this.button_handler(state)})
		new Gpio(this.setting.rotary_pin_1, 'in', 'both').watch((err, state) => {this.rotary_handler(state, 0)})
		new Gpio(this.setting.rotary_pin_2, 'in', 'both').watch((err, state) => {this.rotary_handler(state, 1)})
	}

	button_handler(state) {
		let now = new Date()
		clearTimeout(this.back_timer)
		if (state) {
			// release
			if (this.back_timer && now - this.last_time > 50) {
				events.emit('input', 'enter')
			}
		} else {
			// press
			this.last_time = now
			this.back_timer = setTimeout(() => {
				this.back_timer = 0
				events.emit('input', 'back')
			}, this.setting.button_back_time)
		}
	}

	rotary_handler(state, pin) {
		this.state[pin] = state
		if (!this.state[0] && !this.state[1]) {
			this.next_state = true
		}
		if (this.next_state && this.state[0] && this.state[1] && this.last_state[0] != this.last_state[1]) {
			let now = new Date()
			this.next_state = false
			let inc = Math.max(1, Math.round(Math.pow(100 / (now - this.last_time), 1.5)))
			this.last_time = now
			if (this.last_state[0]) {
				events.emit('input', 'wheel', -inc)
			}
			if (this.last_state[1]) {
				events.emit('input', 'wheel', inc)
			}
		}
		this.last_state = [...this.state]
	}
}
