const settings = require('../modules/settings')
const events = require('../modules/events')

module.exports = class {
	constructor() {
		this.station = 1
		this.active = false
		this.action = 0
		this.actions = ['Verschieben', 'Löschen']
		this.edit = false
		this.move = 0

		events.on('view', name => {setTimeout(() => {
			this.active = name == 'stations_manage'
			if (this.active) {
				this.show()
			}
		}, 100)})

		events.on('input', (type, value) => {
			if (!this.active) return
			switch(type) {
				case 'enter':
					if (this.edit) {
						if (this.action == 0) {
							if (this.move) {
								let station = settings.stations.splice(this.move, 1)[0]
								settings.stations.splice(this.station + (this.move <= this.station ? 0 : 1), 0, station)
								this.station += this.move <= this.station ? 0 : 1
								this.move = 0
								this.edit = false
							} else {
								this.move = this.station
							}
						}
						if (this.action == 1) {
							settings.stations.splice(this.station, 1)
							settings.save({stations: settings.stations})
							this.station = Math.min(this.station, settings.stations.length - 1)
							this.edit = false
						}
					} else {
						this.edit = true
						this.move = null
					}
					this.show()
					break
				case 'back':
					if (this.edit) {
						if (this.move) {
							this.station = this.move
							this.move = 0
						} else {
							this.edit = false
						}
						this.show()
					} else {
						events.emit('view', 'stations')
					}
					break
				case 'wheel':
					if (this.edit && !this.move) {
						this.action = Math.max(0, Math.min(this.action + value, this.actions.length - 1))
						this.show()
					} else {
						this.station = Math.max(this.move ? 0 : 1, Math.min(this.station + value, settings.stations.length - 1))
						this.show()
					}
					break
			}
		})
	}

	show() {
		events.emit('render', display => {
			display.clear()
			display.text('Title', 'Sender verwalten')

			if (!this.edit) {
				display.text('Text', settings.stations[this.station].name, 1, 1, true)
			} else {
				if (!this.move) {
					display.text('Text', settings.stations[this.station].name)
					display.text('Text', this.actions[this.action], 1, 2, true)
				} else {
					display.text('Text', settings.stations[this.move].name)
					display.text('Text', 'einfügen hinter', 1, 2)
					display.text('Text', settings.stations[this.station].name, 1, 3, true)
				}
			}
		})
	}
}
