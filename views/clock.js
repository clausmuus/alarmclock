const settings = require('../modules/settings')
const events = require('../modules/events')
const date = require('date-and-time')
date.locale(require(`date-and-time/locale/${settings.locale}`))

module.exports = class {
	constructor() {
		this.state = 0
		// this.states = [{show: this.show_clock, view: 'radio'}, {show: this.show_alarm, view: 'alarm times'}, {show: this.show_timer, view: 'timer'}, {show: this.show_sleep, view: 'sleep'}, {show: this.show_temperature}, {show: this.show_settings, view: 'settings'}]
		this.states = [{show: this.show_clock, view: 'radio'}, {show: this.show_alarm, view: 'alarm times'}, {show: this.show_timer, view: 'timer'}, {show: this.show_sleep, view: 'sleep'}, {show: this.show_settings, view: 'settings'}]
		this.active = false
		this.brightness = 1

		events.on('view', name => {setTimeout(() => {
			this.active = name == 'clock'
			if (this.active) {
				this.state = 0
				this.show()
			} else {
				clearTimeout(this.fallback_timer)
			}
		}, 100)})

		events.on('update', () => {
			this.show()
		})

		events.on('input', (type, value) => {
			// console.log(type, value || '')
			if (!this.active) return
			clearTimeout(this.fallback_timer)
			switch(type) {
				case 'enter':
					if (this.states[this.state].view) {
						events.emit('view', this.states[this.state].view)
					}
					break
				case 'back':
					this.state = 0
					this.show()
					break
				case 'wheel':
					this.state = Math.max(0, Math.min(this.state + value, this.states.length - 1))
					this.show()
					clearTimeout(this.fallback_timer)
					if (this.state != 0) {
						this.fallback_timer = setTimeout(() => {
							this.state = 0
							this.show()
						}, 1000 * settings.fallback_time)
					}
					break
			}
		})

		events.on('timer', time => {
			this.timer = Date.now() + 1000*60*time
		})

		events.on('temperatures', temperatures => {
			this.temperatures = temperatures
			this.show()
		})
	}

	show() {
		if (this.active) {
			events.emit('render', this.states[this.state].show.bind(this))
		}
	}

	show_clock(display) {
		display.clear()
		display.text('Time', date.format(new Date(), 'H:mm'), 0)

		if (display.brightness) {
			if (this.temperatures && this.temperatures.local !== undefined) {
				display.text('Text', date.format(new Date(), 'dd DD.MM'), settings.alarm.times.find(alarm => alarm.active) ? 2.2 : 1, 3)
				display.text('Text', `${this.temperatures.local.toFixed(1)}°C`, -1, 3)
			} else {
				display.text('Text', date.format(new Date(), 'dd DD.MM'), 0, 3)
			}

			if (settings.alarm.times.find(alarm => alarm.active)) {
				display.text('Text', '!', 1, 3)
			}
		} else {
			display.darkmode = true
		}
	}

	show_alarm(display) {
		display.clear()
		display.text('Title', 'Weckzeiten')

		if (settings.alarm.times.filter(alarm => alarm.active).length) {
			let timeToNextAlarm = alarm => {
				let now = new Date()
				let current = ((now.getDay()+6)%7)*24*60 + now.getHours()*60 + now.getMinutes()
				let next = 7*24*60
				for(let day=0; day<7; day++) {
					if (alarm.days[day]) {
						let minutes = ((day*24*60+alarm.hour*60+alarm.minute) - current + 7*24*60) % (7*24*60)
						next = minutes < next ? minutes : next
					}
				}
				return next
			}
			let top = 0
			settings.alarm.times.filter(alarm => alarm.active).sort((a, b) => timeToNextAlarm(a) - timeToNextAlarm(b)).forEach(alarm => {
				top++
				display.text('Text', `${alarm.hour}:${alarm.minute.toString().padStart(2, '0')}`, alarm.hour<10 ? 2 : 1, top)
				if (alarm.active === 1) {
					display.text('Small', 'Einmalig', 6.2, top)
				} else {
					let days = []
					alarm.days.forEach((day, key) => day && days.push(date._parser.res.dd[(key+1)%7]))
					display.text('Small', days.join(' '), 6.2, top)
				}
			})
		} else {
			display.text('Text', 'Keine Weckzeiten')
		}
	}

	show_timer(display) {
		display.clear()
		display.text('Title', 'Kurzzeitwecker')

		let time = Math.max(0, Math.ceil((this.timer - Date.now())/1000/60))
		display.text('Text', time ? `In ${time} Minuten wecken` : 'Ausgeschaltet')
	}

	show_sleep(display) {
		display.clear()
		display.text('Title', 'Ausschalttimer')

		display.text('Text', 'Ausschalttimer setzen')
	}

	show_temperature(display) {
		display.clear()
		display.text('Title', 'Temperatur')

		if (this.temperatures && this.temperatures.local !== undefined) {
			display.text('Text', `Innen: ${this.temperatures.local.toFixed(1)}°C`)
		}
		if (this.temperatures && this.temperatures.outsite) {
			display.text('Text', `Außen: ${this.temperatures.outsite.toFixed(1)}°C`, 1, 2)
		}
	}

	show_settings(display) {
		display.clear()
		display.text('Title', 'Einstellungen')
		display.text('Text', 'Wecker, Radio,...')
	}
}
