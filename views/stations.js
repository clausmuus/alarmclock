const settings = require('../modules/settings')
const events = require('../modules/events')

module.exports = class {
	constructor() {
		this.station = settings.station
		this.stations = [...settings.stations.filter(station => !station.ringtone), {show: this.show_search, view: 'stations_search'}, {show: this.show_manage, view: 'stations_manage'}]
		this.active = false

		events.on('view', name => {setTimeout(() => {
			this.active = name == 'stations'
			if (this.active) {
				this.show()
			}
		}, 100)})

		events.on('input', (type, value) => {
			if (!this.active) return
			switch(type) {
				case 'enter':
					if (this.stations[this.station].url) {
						events.emit('station', this.station)
						events.emit('view', 'radio')
					}
					if (this.stations[this.station].view) {
						events.emit('view', this.stations[this.station].view)
					}
					break
				case 'back':
					events.emit('view', 'radio')
					break
				case 'wheel':
					this.station = Math.max(0, Math.min(this.station + value, this.stations.length - 1))
					this.show()
					break
			}
		})

		events.on('station', station => {
			this.station = station
		})

		events.on('settings', () => {
			this.station = settings.station
			this.stations = [...settings.stations.filter(station => !station.ringtone), {show: this.show_search, view: 'stations_search'}, {show: this.show_manage, view: 'stations_manage'}]
		})

		events.on('sleep', time => {
			this.sleep_time = time && new Date()/1000/60 + time
		})
	}

	show() {
		if (this.active) {
			events.emit('render', (this.stations[this.station].show || this.show_station).bind(this))
		}
	}

	show_station(display) {
		display.clear()
		display.text('Title', 'Sender')

		display.text('Text', this.stations[this.station].name, 0, 1, false, true)
	}

	show_search(display) {
		display.clear()
		display.text('Title', 'Sender suchen')

		display.text('Text', 'Einen neuen Sender')
		display.text('Text', 'suchen', 1, 2)
	}

	show_manage(display) {
		display.clear()
		display.text('Title', 'Sender verwalten')

		display.text('Text', 'Sender verschieben')
		display.text('Text', 'oder löschen', 1, 2)
	}
}
