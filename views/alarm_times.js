const settings = require('../modules/settings')
const events = require('../modules/events')
const date = require('date-and-time')
date.locale(require(`date-and-time/locale/${settings.locale}`))

module.exports = class {
	constructor() {
		this.alarm = 0
		this.edit = 0
		this.active = false
		this.state = 2
		this.states = [{name: 'Löschen', value: null}, {name: 'Einmal', value: 1}, {name: 'Aktiv', value: true}, {name: 'Inaktiv', value: false}]

		events.on('view', name => {setTimeout(() => {
			this.active = name == 'alarm times'
			if (this.active) {
				this.show()
			}
		}, 100)})

		events.on('input', (type, value) => {
			if (!this.active) return
			let alarm = settings.alarm.times[this.alarm]
			switch(type) {
				case 'enter':
					if (!alarm) {
						// create new alarm
						settings.alarm.times.push({
							"active": true,
							"hour": 6,
							"minute": 0,
							"days": [1,1,1,1,1,1,1]
						})
						this.edit = 1
						this.state = 2
					} else if (alarm.active === null) {
						// delete current alarm
						settings.alarm.times.splice(this.alarm, 1)
						this.edit = 0
					} else {
						// select next value
						this.edit = (this.edit + 1) % (this.state === 1 ? 4 : 11)
						if (this.edit) {
							this.state = this.states.findIndex(state => state.value === alarm.active)
						} else {
							if (alarm.active === 1) {
								alarm.days = [1,1,1,1,1,1,1]
							}
							settings.save({alarm:{times: settings.alarm.times}})
						}
					}
					this.show()
					break
				case 'back':
					if (this.edit) {
						if (alarm.active === null) {
							// delete current alarm
							settings.alarm.times.splice(this.alarm, 1)
						}
						// save
						settings.save({alarm:{times: settings.alarm.times}})
						this.edit = 0
						this.show()
					} else {
						events.emit('view', 'clock')
					}
					break
				case 'wheel':
					switch(this.edit) {
						case 0:
							this.alarm = Math.max(0, Math.min(this.alarm + value, settings.alarm.times.length))
							break
						case 1:
							this.state = Math.max(0, Math.min(this.state + value, this.states.length - 1))
							alarm.active = this.states[this.state].value
							break
						case 2:
							alarm.hour = (alarm.hour + value + 24) % 24
							break
						case 3:
							alarm.minute = (alarm.minute + value + 60) % 60
							break
						default:
							alarm.days[this.edit - 4] = !alarm.days[this.edit - 4]
							break
					}
					this.show()
					break
			}
		})
	}

	show() {
		events.emit('render', display => {
			display.clear()
			display.text('Title', 'Weckzeiten')

			let alarm = settings.alarm.times[this.alarm]
			if (!alarm) {
				display.text('Text', 'Weckzeit hinzufügen')
			} else {
				display.text('Text', this.states.find(state => state.value === alarm.active).name, 1, 1, this.edit == 1)

				if (alarm.active !== null) {
					display.text('Text', alarm.hour.toString().padStart(2, '0'), 1, 2, this.edit == 2)
					display.text('Text', ':', 3, 2)
					display.text('Text', alarm.minute.toString().padStart(2, '0'), 3.5, 2, this.edit == 3)

					if (alarm.active !== 1) {
						for (let day=0; day<7; day++) {
							display.text('Text', alarm.days[day] ? date._parser.res.dd[(day+1)%7] : '-', day * 2.25 + (alarm.days[day] ? 1 : 1.5), 3, this.edit == 4+day)
						}
					}
				}
			}
		})
	}
}
