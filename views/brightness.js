const settings = require('../modules/settings')
const events = require('../modules/events')
const linearize = require('../modules/linearize')
const I2c = require('i2c')
const rpio = require('rpio')

module.exports = class {
	constructor() {
		let setting = settings.brightness_sensor

		rpio.init({mapping: 'gpio'})
		if (setting.ground_pin) {
			rpio.open(setting.ground_pin, rpio.OUTPUT)
			rpio.write(setting.ground_pin, rpio.LOW)
		}
		if (setting.vcc_pin) {
			rpio.open(setting.vcc_pin, rpio.OUTPUT)
			rpio.write(setting.vcc_pin, rpio.HIGH)
		}

		let i2c = new I2c(setting.address, {device: setting.device})
		i2c.scan((err, data) => {
			if (err) {
				console.log(err)
			} else if (!data.includes(setting.address)) {
				console.log('Wrong brightness sensor address. Available i2c addresses are:', data)
			} else {
				let last_brightness = 255
				let last_dark = false
				setInterval(() => {
					i2c.readBytes(0x20, 2, (err, res) => {
						let light = res[0]*256 + res[1]
						let brightness = linearize(255 / setting.max * Math.max(setting.min, light), 255, setting.linearize)
						if (Math.abs(brightness - last_brightness) / Math.pow(brightness, 0.5) > 0.5) {
							last_brightness = brightness
							events.emit('brightness', Math.round(brightness))
						}
					})
				}, 1000)
			}
		})
	}
}
