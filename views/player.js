const settings = require('../modules/settings')
const events = require('../modules/events')
const linearize = require('../modules/linearize')
const StupidPlayer = require('stupid-player').StupidPlayer

module.exports = class {
	constructor() {
		this.player = new StupidPlayer()
		this.playing = false

		this.player.on(this.player.EVENT_STOP, e => {
			if (this.playing) {
				this.play()
			}
		})

		this.player.on(this.player.EVENT_ERROR, e => {
			console.log(e)
			if (this.playing) {
				this.play_timer = setTimeout(this.play.bind(this), 5000)
			}
		})

		events.on('play', (station, fallback) => {
			if (station !== undefined) {
				if (station != this.station) {
					this.stop()
				}
				this.station = station
				this.fallback = fallback !== undefined ? fallback : (station !== undefined ? station : this.fallback)
				this.playing = true
			}
			this.play()
		})

		events.on('stop', () => {
			this.playing = false
			this.stop()
		})

		events.on('volume', volume => {
			this.volume = volume
			this.setVolume()
		})
	}

	play() {
		clearTimeout(this.play_timer)
		if (this.player.getState() != 'play') {
			StupidPlayer.getReadStream(settings.stations[this.station].url).catch(e => {
				console.log(e)
				events.emit('message', 'Wiedergabe Fehler!')
				this.station = this.fallback
				this.play_timer = setTimeout(this.play, 5000)
			}).then(stream => {
				this.error = ''
				if (!stream) {
					console.log('error: empty stream')
					events.emit('message', 'Wiedergabe Fehler!')
				} else {
					this.player.play(stream)
					this.setVolume()
				}
			})
		}
	}

	stop() {
		clearTimeout(this.play_timer)
		this.player.stop()
	}

	setVolume() {
		if (this.player.getState() == 'play') {
			this.player.setVolume(linearize(this.volume * (settings.volume.max - settings.volume.min) / settings.volume.steps + settings.volume.min, settings.volume.max, settings.volume.linearize))
		}
	}
}
