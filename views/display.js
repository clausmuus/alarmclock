const settings = require('../modules/settings')
const events = require('../modules/events')
const Display = require('oled-display')

module.exports = class {
	constructor() {
		const display = new Display(settings.display, __dirname)

		Object.entries(settings.styles).forEach(([name, font]) => {
			display.loadFont(font.file || settings.display.font, name, font.size)
		})

		let width = display.getWidth()
		let height = display.getHeight()
		display.mash = []
		for (let x=0; x < width; x+=2) {
			for (let y=0; y < height; y++) {
				display.mash.push(x + y%2, y)
			}
		}

		display.brightness = settings.display.brightness.max
		display._render = display.render
		display._renderer = null
		display._rendering = false
		display.render = async function(call) {
			call && clearTimeout(this.active_timer)
			if(!this._rendering) {
				this._renderer = null
				this._rendering = true
				call && (this.darkmode = false)
				call && await call(this)
				this.darkmode && this.drawPoints(0, ...this.mash)
				this.setBrightness(Math.max(this.darkmode ? settings.display.brightness.dark : settings.display.brightness.min, this.brightness))
				await this._render(10000)
				this._rendering = false
				if (this._renderer != null) {
					this.render(this._renderer)
				}
			} else if (call || !this._renderer) {
				this._renderer = call
			}
		}

		display.text = function(style, text, col = 1, row = 1, active = false, wrap = false) {
			let align = (col > 0 ? 1 : (col < 0 ? -1 : 0))
			let left = (col - align) * (settings.styles[style].width || 0) * align + (align == 1 ? 0 : (align == -1 ? this.getWidth() : this.getWidth()/2))
			let top = (row - 1) * (settings.styles[style].height || 0) + (settings.styles[style].top || 0)
			this.setFont(style)
			this.drawText(1, text, left, top, align, 1, wrap)

			if (active) {
				let draw = async () => {
					this.render(async () => {
						active = !active
						this.setFont(style)
						this.drawText(active, text, left, top, align)
						this.active_timer = setTimeout(draw, active ? 300 : 30)
					})
				}
				draw()
			}
		}

		events.on('render', display.render.bind(display))

		events.on('brightness', brightness => {
			display.brightness = brightness
			events.emit('update')
			display.render()
		})

		// Draw border
		// this.render(display => {
		// 	display.drawRectangleSize(0, 1, 0, 0, display.getWidth() - 1, display.getHeight() - 1)
		// })
	}
}
