const settings = require('../modules/settings')
const events = require('../modules/events')
const RadioBrowser = require('radio-browser')


module.exports = class {
	constructor() {
		this.state = 0
		this.states = [this.show_countries, this.show_letters, this.show_names]
		this.country = 0
		this.countries = []
		this.letter = 0
		this.letters = null
		this.station = 0
		this.stations = null
		this.all_stations = null
		this.active = false

		RadioBrowser.getCategory('countries').then(countries => {
			this.countries = countries.sort((a, b) => a.name.localeCompare(b.name))
			this.country = this.countries.findIndex(country => country.name == 'Germany')
		})

		events.on('view', name => {setTimeout(() => {
			this.active = name == 'stations_search'
			if (this.active) {
				this.show()
			}
		}, 100)})

		events.on('input', async (type, value) => {
			if (!this.active) return
			switch(type) {
				case 'enter':
					switch(this.state) {
						case 0:
							this.state++
							if (!this.all_stations) {
								RadioBrowser.searchStations({country: this.countries[this.country].name}).then(stations => {
									this.all_stations = stations
									this.letters = new Set()
									stations.forEach(station => {if (station.name[0]>' ' && station.name[0]<='~') this.letters.add(station.name[0].toUpperCase())})
									this.letters = [...this.letters]
									this.letter = Math.max(0, this.letters.findIndex(letter => letter >= 'A' && letter <= 'Z'))
									this.show()
								})
							}
							this.show()
							break
						case 1:
							if (this.letters) {
								this.state++
								this.stations = this.all_stations.filter(station => station.name.toUpperCase().startsWith(this.letters[this.letter]))
								this.show()
							}
							break
						case 2:
							let station = settings.stations.filter(station => !station.ringtone).findIndex(station => station.name == this.stations[this.station].name)
							if (station == -1) {
								settings.stations.push({name: this.stations[this.station].name, url: this.stations[this.station].url_resolved})
								await settings.save({stations: settings.stations})
								station = settings.stations.filter(station => !station.ringtone).length - 1
							}
							events.emit('station', station)
							events.emit('view', 'radio')
							break
					}
					break
				case 'back':
					switch(this.state) {
						case 0:
							events.emit('view', 'radio')
							break
						default:
							this.state--
							this.show()
					}
					break
				case 'wheel':
					switch(this.state) {
						case 0:
							this.country = Math.max(0, Math.min(this.country + value, this.countries.length - 1))
							this.all_stations = null
							this.stations = null
							this.letters = null
							break
						case 1:
							if (this.letters) {
								this.letter = Math.max(0, Math.min(this.letter + value, this.letters.length - 1))
								this.station = 0
							}
							break
						case 2:
							this.station = Math.max(0, Math.min(this.station + value, this.stations.length - 1))
							break
					}
					this.show()
					break
			}
		})
	}

	show() {
		if (this.active) {
			events.emit('render', this.states[this.state].bind(this))
		}
	}

	show_countries(display) {
		display.clear()
		display.text('Title', 'Sender suchen')

		display.text('Text', 'Land')
		display.text('Text', this.countries[this.country].name, 1, 2, true)
	}

	show_letters(display) {
		display.clear()
		display.text('Title', 'Sender suchen')

		if (this.all_stations) {
			display.text('Text', 'Name beginnt mit')
			display.text('Text', this.letters[this.letter], 1, 2, true)
		} else {
			display.text('Text', 'Lade Senderliste...', 1, 2)
		}
	}

	show_names(display) {
		display.clear()
		display.text('Title', 'Sender suchen')

		display.text('Text', 'Name')
		display.text('Text', this.stations[this.station].name, 1, 2, false, true)
	}
}
