const settings = require('../modules/settings')
const events = require('../modules/events')
const date = require('date-and-time')

module.exports = class {
	constructor() {
		this.station = settings.radio.station
		this.volume = settings.radio.volume
		this.active = false

		events.on('view', name => {setTimeout(() => {
			this.show_volume = false
			this.active = name == 'radio'
			if (this.active) {
				this.show()
				events.emit('volume', this.volume)
				events.emit('play', settings.stations.map((station, id) => ({...station, id})).filter(station => !station.ringtone)[this.station].id)
				events.emit('station', this.station)
			} else {
				clearTimeout(this.show_volume_timer)
			}
		}, 100)})

		events.on('update', () => {
			if (this.active) {
				this.show()
				events.emit('play')
			}
		})

		events.on('input', (type, value) => {
			if (!this.active) return
			clearTimeout(this.show_volume_timer)
			switch(type) {
				case 'enter':
					this.active = false
					events.emit('view', 'stations')
					break
				case 'back':
					events.emit('stop')
					clearTimeout(this.sleep_timer)
					this.sleep_timer = 0
					this.station = settings.radio.station
					this.volume = settings.radio.volume
					events.emit('view', 'clock')
					break
				case 'wheel':
					this.volume = Math.max(0, Math.min(this.volume + value, settings.volume.steps))
					this.show_volume = true
					this.show_volume_timer = setTimeout(() => {this.show_volume = false; this.show()}, 1000)
					events.emit('volume', this.volume)
					this.show()
					break
			}
		})

		events.on('station', station => {
			this.station = station
		})

		events.on('settings', () => {
			this.station = settings.radio.station
			this.volume = settings.radio.volume
		})

		events.on('sleep', time => {
			clearTimeout(this.sleep_timer)
			this.sleep_timer = 0
			if (time) {
				this.sleep_timer = setTimeout(() => {
					events.emit('stop')
					events.emit('view', 'clock')
				}, 1000*60*time)
			}
		})

		events.on('message', message => {
			this.show(message)
		})
	}

	show(message) {
		events.emit('render', display => {
			display.clear()
			display.text('Time', date.format(new Date(), 'H:mm'), 0)

			if (this.show_volume) {
				let width = display.getWidth()
				display.drawRectangleSize(1, 1, 0, 45, width / settings.volume.steps * this.volume, 2)
			}

			display.text('Text', message ? message : settings.stations.filter(station => !station.ringtone)[this.station].name, 0, 3)

			if (this.sleep_timer) {
				display.text('Text', '!', 1, 3)
			}

			display.darkmode = !display.brightness
		})
	}
}
