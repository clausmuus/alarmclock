# Radiowecker
Ein Radiowecker auf RPI Zero W Basis mit einem SSD1309 128x64px 2.4" OLED Display und Drehimpulsgeber zur Steuerung.
Als Weckton und zum Einschlafen können Internetradio Sender oder lokale mp3 Sounds gewählt werden.
Als Gehäuse wird das des Lautsprechers verwendet.

<img src="photos/front.jpg"/>

## Bedienung
Ausgangsanzeige ist die Uhrzeit, das Datum und die Raumtemperatur. Eine Markierung in der linken unteren Ecke zeigt eine aktive Weckzeit an. Ein Druck auf den Knopf schaltet das Radio ein. Mit dem Drehrad wird zwischen Uhrzeit, Weckzeiten, Kurzzeitwecker und Einstellungen gewechselt.

Bei eingeschaltetem Radio zeigt der Bildschirm die Uhrzeit und den aktiven Sender an. Das Drehrad ändert hier die Lautstärke. Nach einem Druck auf den Knopf kann der Sender gewählt werden.

In allen anderen Zuständen zeigt der Bildschirm die aktive Funktion an. Dann kann mit dem Drehrad die gewünschte Funktion ausgewählt oder eine Einstellung geändert werden. Mit Druck auf den Knopf wird die gewählte Funktion aktiviert. Langes Drücken führt einen Schritt zurück.

Wenn der Alarm einschaltet, wird der gewählte Sender abgespielt, anfangs leise, dann mit ansteigender Lautstärke. Ein Druck auf den Knopf pausiert den Alarm für einige Minuten. Ein langes drücken des Knopfes schaltet den Alarm ganz aus.

## Besonderheiten
Die Helligkeit des Displays passt sich an das Umgebungslicht an und dunkelt nachts so stark ab, dass dieses gerade noch ablesbar ist. Die Umebung des Weckers wird deshalb nachts nicht beleuchtet. Tagsüber ist das Display auch bei direkter Sonneneinstralung gut ablesbar.

Das Wecken beginnt sehr leise und steigert sich langsam, bis auf die eingestellte Maximallautstärke.

Der Lautstärkeregler hat über den gesammten Bereich einen linearen Verlauf. Dadurch ist die empfundene Lautstärkeregelung über den gesammten Bereich gleichmäßg abgestuft.

Für die Anzeige kann jede beliebige Schrift verwendet werden.

Der Wecker hat einen sehr niedrigen Stromverbrauch von ca 0.7W.

## Inspiration
Ich habe diesen Wecker gebaut, weil ich für meine Frau kein fertiges Gerät finden konnte, dass die für mich wichtigsten Eigenschaften erfüllt.

- gut ablesbare Anzeige, die nachts nicht den Raum erläuchtet
- intuitive Bedienung (wenige Tasten)
- guter Klang
- rauschfreier Empfang und vielfältige Senderauswahl (Internetradio)

## Arbeitszeit
Von Begin der Planungen bis zum fertigen Gerät sind ca. 4 Wochen vergangen. Die reinen handwerklichen Tätigkeiten (löten, sägen, feilen) haben ca. zwei Tage in Anspruch genommen.
Die Programmierung hat ca. zwei Wochen Arbeitszeit gebraucht. Die Hälfte dieser Zeit nahm das Ansprechen der vielfältigen Hardware in Anspruch.

## Webinterface
Das integrierte Webinterface bietet bisher nur rudimentäre funktionen (Anzeige der Weckzeiten). Dies dient nur als Beispiel für die Verwendung der integrieten API und könnte bei Bedarf leicht erweitert werden.

## Raspberry Pi OS einrichten

### Image
Raspberry Pi OS bullseye Headless Image herunterladen und auf eine SD-Karte kopieren.

#### SPI aktivieren
In der ```boot/config.txt``` diese Zeilen hinzufügen:
```
dtparam=audio=on
dtparam=spi=on
dtparam=i2c_arm=on
dtoverlay=w1-gpio,gpiopin=18
```

#### WLAN einrichten
Die Datei ```boot/wpa_supplicant.conf``` mit diesem Inhalt erstellen:
```
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
Update_config=1
Country=DE

Network={
	ssid="YourWifiNetwork"
	psk="WifiPassword"
}
```

#### SSH aktivieren
Die leere Datei ```boot/ssh``` erstellen

## System einrichten
RPI mit Image starten

### Python installieren
```bash
sudo apt install python3 python3-pip git libasound2-dev
sudo pip3 install --upgrade luma.oled
```

### Soundkarte einrichten
Die Datei ```/etc/asound.conf``` mit folgendem Inhalt erstellen:
```
pcm.!default {
  type rate
  slave {
    pcm {
      type hw
      card "UACDemoV10"
      device 0
    }
     rate 48000
  }
#  type hw
#  card "UACDemoV10"
#  device 0
}
```

### i²c Zugriff erlauben
```
sudo adduser pi i2c
sudo echo "i2c-dev" >>/etc/modules
```

## Wecker einrichten

### Node installieren
```bash
sudo apt install node npm
```

### Wecker Software herunterladen
```bash
git clone https://gitlab.com/clausmuus/alarmclock.git
```

### Abhängigkeiten installieren
```bash
cd alarmclock
npm install
```

### Patches einspielen
Alle Patches im Ordner patches einspielen.
```
patch -p 0 -i patches/oled-display.patch
patch -p 0 -i patches/raspi-1wire-temp.patch
patch -p 0 -i patches/stupid-player.patch
```

### Wecker starten
```bash
npm run start
```
oder als Service starten
```
cp alarmclock.service /lib/systemd/system
systemctl start alarmclock
systemctl enable alarmclock
```

### Konfiguration
Die Wecker Voreinstellungen und die Konfiguration der Hardware erfolgt in der Datei ```modules/settings/defaults.json```. Änderungen die über das Konfigurationsmenü erfolgen, werden in der Datei ```modules/settings/local.json``` gespeichert.

## Zusammenbau und Verkabelung

<img src="photos/front_closed.jpg" width="50%"/><img src="photos/back.jpg" width="50%"/>
<img src="photos/front_inside.jpg" width="50%"/><img src="photos/inside.jpg" width="50%"/>

### Spannungsversorgung
Für die Spannungsversorgung kann das Anschlusskabel des Lautsprechers verwendet werden (alternativ ```Micro USB Power IN``` des RPI).
```
Netzteil    RPI
VCC         2   5V
GND         6   GND
```

### RaspberyPI Zero W
Der RPI wird unter dem linken Lautsprecer plaziert. Dafür werden in die Lautsprecher Befestigungsstege, mit einem Lötkolben, kleine Kerben geschmolzen.

Für die Verbindung mit dem GPIO Ports kann keine Steckverbindung (Pfostenleiste) verwendet werden, da dafür nicht genug Platz ist. Alle Kabel müssen also am RPI angelötet werden. Lediglich das Spannungsversorgungskabel ist gesteckt.

Die verwendeten Kabel haben den entscheidenen Nachteil, dass diese viel zu steif sind. Dadurch gestaltet sich das finale zusammensetzen recht schwierig, und mir sind dabei dann auch zwei Kabel abgebrochen. Ich empfehle lieber auf die Steckverbindungen zu verzichten, und dafür dünnere Kabel zu verwenden.

### Display
Für das Display wird die mittlere Öffung des Lautsprechers, mit einer Laubsäge etwas vergrößert und das Display hinter die Front geschraubt.

Das Metallgitter vor dem Display wird mit einer Laubsäge mit einem sehr feinen Metall Segeblatt ausgeschnitten.  
```
SSD1309     RPI
GND         20  GND
VCC         17  3.3V
SCL         23  SCLK
SDA         19  MOSI
RES         18  GPIO24
DC          22  GPIO25
CS          24  CE0
```

### Helligkeitssensor
Für den Helligkeitssensor ist am rechten Rand ein 3mm Loch gebort. Der Sensor wird von innen, mit Heißkleber dahinter geklebt.
```
BH1750      RPI
VCC         1   3.3V
GND         7   GPIO4
SCL         5   SCL
SDA         3   SDA
ADDR
```

### Temperatursensor
Der Temperatursensor wird von außen mit durch die Öffnung für das Stromversorgungskabel gesteckt. Der Fühler befindet sich so außerhalb des Gehäuses, steht aber nicht über. Innerhalb des Gehäuses ist eine Messung der Raumtemperatur nicht möglich.
```
DS18B20     RPI
VCC         10  GPIO15
Data        12  GPIO18
GND         14  GND
```

### Drehimpulsgeber
Für das Stellrad des Drehimpulsgebers wird mit einer Laubsäge, auf der Rückseite des Gehäuses ein Schlitz gesägt. Der Drehimpulsgeber wird an einem Winkel verschraubt. Ein verkleben ist nicht sinnvoll, da die Drehimpulsgeber erfahrungsgemäß nicht ewig halten. Der Drehimpulsgeber kann nicht mittig plaziert werden, da der Taster mittig plaziert werden soll.
```
EC11        RPI
A           11  GPIO17
GND         9   GND
B           13  GPIO27
```

### Taster
Für den Taster wird auf der Oberseite des Gehäuses, mittig ein Loch gebohrt. Es muss da drauf geachtet werden, das dieser nicht mit dem Drehimpulsgeber kollidiert.
```
Taster      RPI
A           15  GPIO22
B           9   GPIO23
```

### Lautsprecher
Die Platine des Lautsprechers bleibt an ihrem ursprünglichen platz. Dessen Anschlusskabel wird für die Stromversorgung des RPI verwendet.

Lötpins auf der Rückseite des RPI bei den USB Ports (alternativ ```Micro USB OTG``` des RPI).
```
Lautsprecher  RPI
VCC           5V  PP1
D-            DM  PP23
D+            DP  PP22
GND           GND PP6
```

## Menu Struktur
Diese Bedienungs-Struktur ist nicht vollstendig, sondern stellt lediglich den ursprünglichen Plan dar.
```
Normal:
	Anzeige - Uhrzeit, Datum und Temperatur
	Dreh - Uhrzeit, Weckzeiten, Kurzzeitwecker, Einstellungen
		Anzeige - Auswahl
	Druck - Uhrzeit -> Radio-on
		Anzeige - Uhrzeit und Sender
		Dreh - Lautstärke 
			Anzeige - Uhrzeit und Lautstärke
		Druck -> Sender
			Dreh - Sender 1, Sender ..., Auto-off
				Anzeige - Sender
			Druck - Sender -> Sender Wechseln
			Druck - Auto-off -> Auto-off Zeit einstellen
				Dreh - Auto-off Zeit ändern
				Druck - übernehmen
				Druck lang -> Zurück
			Druck lang -> Zurück
		Druck lang -> Zurück (Radio-off)
	Druck - Weckzeiten -> Weckzeiten-Einstellen
		Anzeige - Aktive Weckzeiten
		Dreh - Weckzeit 1, Weckzeit ..., Neue Weckzeit, Timer
			Anzeige - Auswahl
		Druck - Weckzeit -> Weckzeit einstellen
			Dreh - Ein/Aus/Löschen, Stunde, Minute, Wochentag
				Anzeige - Auswahl hervorheben/blinken
			Druck -> Einstellen
				Dreh - ändern
				Druck -> übernehmen
				Druck lang -> Zurück (Änderung verwerfen)
			Druck lang -> Zurück
		Druck - Timer -> Timer Einstellen
			Dreh - Zeit ändern
			Druck -> übernehmen
			Druck lang -> Zurück (Änderung verwerfen)
		Druck lang -> Zurück
		Druck - Sender -> Weck Sender einstellen
			Dreh - Sender 1, Sender ...
			Druck -> übernehmen
			Druck lang -> Zurück (Änderung verwerfen)
		Druck - Lautstärke -> Weck Lautstärke einstellen (Radio-on)
			Dreh - Lautstärke
			Druck -> übernehmen (Radio-off)
			Druck lang -> Zurück (Änderung verwerfen) (Radio-off)
		Druck lang -> Zurück
	Druck - Temperatur -> Temperaturen
		Anzeige - Innen und außen Temperatur
		Dreh - Raum Auswahl (Innen, Außen)
			Anzeige - Auswahl
		Druck lang -> Zurück
	Druck - Einstellungen -> Einstellungen ändern
		Anzeige - Sender, Lautstärke, Alarm,...
		Dreh - Einstellung auswählen
			Anzeige - Auswahl
		Druck -> Einstellung ändern
			Anzeige - Wert blinkt
			Dreh - ändert Wert
			Druck -> übernehmen
			Druck lang -> Zurück (Änderung verwerfen)
		Druck lang -> Zurück

Alarm -> Radio-on
	Anzeige - Uhrzeit und Weckzeit
	Dreh - Lautstärke 
		Anzeige - Uhrzeit und Lautstärke
	Druck -> Radio-off für 10 Minuten
	Druck lang -> Alarm-Off (Radio-off)
```

## Software
Die Software des Weckers ist in JavaScript geschrieben und verwendet NodeJS als Plattform.
Ich habe diese Sprache gewählt, da mir diese am geläufigsten ist.
Der Wecker ist modular aufgebaut, so dass er sich sehr leicht erweitern oder an eigene Wünsche anpassen lässt.

## Hardware Einkaufsliste
- Raspberry Pi Zero W
	- https://www.berrybase.de/raspberry-pi-zero-w
	- 15€
- OLED Display
	- https://www.amazon.de/dp/B09Z299L36
	- 20€
- USB Lautsprecher
	- https://www.amazon.de/dp/B089W5PS29
	- 20€
- Taster
	- https://www.amazon.de/dp/B074H3RRPV
	- 6€
- Drehimpulsgeber
	- https://www.amazon.de/dp/B0BCYSXSF8
	- 2€
- Helligkeitssensor
	- https://www.amazon.de/dp/B07VF15XJJ
	- 2€
- Temperatursensor
	- https://www.amazon.de/dp/B07VB25GS5
	- 1€
- Kabel
	- https://www.amazon.de/dp/B07KYHBVR7
	- 5€

### Kosten
Die Gesammtkosten belaufen sich auf ca. __70€__

### Energiebedarf
Der Wecker hat einen durchschnittlichen Stromverbrauch von 0.7W
