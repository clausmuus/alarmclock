const timeToNextAlarm = alarm => {
	let now = new Date()
	let current = ((now.getDay()+6)%7)*24*60 + now.getHours()*60 + now.getMinutes()
	let next = 7*24*60
	for(let day=0; day<7; day++) {
		if (alarm.days[day]) {
			let minutes = ((day*24*60+alarm.hour*60+alarm.minute) - current + 7*24*60) % (7*24*60)
			next = minutes < next ? minutes : next
		}
	}
	return next
}

const settings = async () => {
	let settings = await (await fetch(new Request('/settings')))?.json()
	if (settings) {
		document.getElementById('alarms').replaceChildren(
			...settings.alarm.times.filter(alarm => alarm.active).sort((a, b) => timeToNextAlarm(a) - timeToNextAlarm(b)).map(alarm => {
				let element = document.createElement('li')
				element.innerHTML = `${alarm.hour.toString().padStart(2, ' ')}:${alarm.minute.toString().padStart(2, '0')} am `
				if (alarm.active === 1) {
					element.innerHTML += 'Einmalig'
				} else {
					let days = []
					alarm.days.forEach((day, key) => day && days.push(new Date(Date.UTC(2017, 0, key+2)).toLocaleDateString(settings.locale, { weekday: 'long' })))
					element.innerHTML += days.join(' ')
				}
				return element
			})
		)

		document.getElementById('stations').replaceChildren(
			...settings.stations.filter(station => !station.ringtone).map(station => {
				let element = document.createElement('li')
				element.innerHTML = `${station.name}`
				return element
			})
		)
	}
}

settings()
setInterval(settings, 1000*60)