const events = require('./modules/events')
const settings = require('./modules/settings')
const dir = require('node-dir')

;(async () => {
	await settings.load()

	// load all views
	dir.files(__dirname + '/views', {sync:true}).filter(file => file.endsWith('.js')).forEach(file => {
		let view = file.replace(/.*\/(.*).js/, '$1')
		if (!settings[view] || !settings[view].disable) {
			console.log(`Load ${view}`)
			new (require(file))()
		}
	})
	// Array('input', 'console', 'clock', 'radio', 'alarm', 'player', 'alarm_times', 'timer', 'sleep', 'settings', 'stations').forEach(view => {
	// 	new (require('./views/'+view))()
	// })
	console.log('Ready')

	events.emit('view', 'clock')

	setTimeout(() => {
		events.emit('update')
		setInterval(() => {events.emit('update')}, 1000 * 10)
	}, 1000 * (61 - new Date().getSeconds()))
})()
